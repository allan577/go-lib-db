module gitee.com/allan577/go-lib-db

go 1.12

require (
	gitee.com/allan577/go-lib-logger v1.0.0
	github.com/BurntSushi/toml v0.3.1
	github.com/go-sql-driver/mysql v1.4.1
	github.com/go-xorm/xorm v0.7.9
	github.com/jinzhu/gorm v1.9.12
	github.com/kr/pretty v0.1.0 // indirect
	github.com/opentracing/opentracing-go v1.1.0
)
