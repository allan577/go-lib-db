package db

import (
	"fmt"

	"github.com/BurntSushi/toml"
)

// ----------------------------------------
//  ORM 配置集合（TOML 支持）
// ----------------------------------------
type ORMConfigs struct {
	Items map[string]*ORMConfig `toml:"orm"`
}

// 绑定数据到当前 ORM 配置清单
func (configs *ORMConfigs) Bind(bs []byte) error {
	if err := toml.Unmarshal(bs, configs); err != nil {
		return fmt.Errorf("the orm configs bind failed: %s", err)
	}
	return nil
}

// ----------------------------------------
//  ORM 配置项（TOML 支持, Viper 支持）
// ----------------------------------------
type ORMConfig struct {
	Engine      string `toml:"engine" mapstructure:"engine"`
	Driver      string `toml:"driver" mapstructure:"driver"`
	Host        string `toml:"host" mapstructure:"host"`
	Port        int    `toml:"port" mapstructure:"port"`
	Username    string `toml:"username" mapstructure:"username"`
	Password    string `toml:"password" mapstructure:"password"`
	Database    string `toml:"database" mapstructure:"database"`
	Charset     string `toml:"charset" mapstructure:"charset"`
	MaxConn     int    `toml:"max_conn" mapstructure:"max_conn"`
	MaxIdleConn int    `toml:"max_idle_conn" mapstructure:"max_idle_conn"`
	Ping        bool   `toml:"ping" mapstructure:"ping"`
}

// 绑定数据到当前 ORM 配置清单
func (config *ORMConfig) Bind(bs []byte) error {
	if err := toml.Unmarshal(bs, config); err != nil {
		return fmt.Errorf("the orm config bind failed: %s", err)
	}
	return nil
}

// 获取当前 ORM 配置项的副本
func (config *ORMConfig) Copy() *ORMConfig {
	return &ORMConfig{
		Engine:      config.Engine,
		Driver:      config.Driver,
		Host:        config.Host,
		Port:        config.Port,
		Username:    config.Username,
		Password:    config.Password,
		Database:    config.Database,
		Charset:     config.Charset,
		MaxConn:     config.MaxConn,
		MaxIdleConn: config.MaxIdleConn,
		Ping:        config.Ping,
	}
}
