# go-lib-db #

基础开发框架数据库组件

## 依赖 ##
- github.com/go-sql-driver/mysql
- github.com/jinzhu/gorm
- github.com/go-xorm/xorm
- github.com/BurntSushi/toml
- git.qutoutiao.net/framework/autumn-lib-configuration
