package db

import (
	"fmt"
	"strings"
)

// ----------------------------------------
//  数据库客户端管理器
// ----------------------------------------
type Database struct {
	gm *GORMClientManager
}

// 创建数据库客户端管理器实例
func CreateDatabase() *Database {
	return &Database{
		gm: CreateGORMClientManager(),
	}
}

// 注册一个 GORM 客户端实例
func (database *Database) RegisterGORMClient(name string, config *ORMConfig) error {
	if client, err := CreateGORMClient(name, config); err != nil {
		return fmt.Errorf("failed to register gorm client %s: %s", name, err)
	} else {
		database.gm.Set(name, client)
		return nil
	}
}

// 注册多个 GORM 客户端实例
func (database *Database) RegisterGORMClients(configs map[string]*ORMConfig) error {
	if len(configs) > 0 {
		for name, config := range configs {
			if err := database.RegisterGORMClient(name, config); err != nil {
				return err
			}
		}
	}
	return nil
}

// 注册一个 ORM 客户端实例（支持 XORM 和 GORM）
func (database *Database) RegisterORMClient(name string, config *ORMConfig) error {
	switch strings.ToUpper(strings.TrimSpace(config.Engine)) {
	case "GORM":
		return database.RegisterGORMClient(name, config)
	default:
		return fmt.Errorf("unsupported orm engine %s", config.Engine)
	}
}

// 注册多个 ORM 客户端实例
func (database *Database) RegisterORMClients(configs map[string]*ORMConfig) error {
	if len(configs) > 0 {
		for name, config := range configs {
			if err := database.RegisterORMClient(name, config); err != nil {
				return err
			}
		}
	}
	return nil
}

// 获取一个已经注册的 GORM 客户端（如果未注册将返回 nil）
func (database *Database) GORM(name string) *GORMClient {
	return database.gm.Get(name)
}
